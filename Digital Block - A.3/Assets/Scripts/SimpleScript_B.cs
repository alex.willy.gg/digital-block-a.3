﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleScript_B : MonoBehaviour
{
    private int enemyDistance = 0;
    private int enemyCount = 10;

    private string[] enemies = new string[6];

    private int weaponId = 0;

    // Start is called before the first frame update
    void Start()
    {
        print("Press 'Space' to Search an Enemy.");
        print("Press 'X' to Destroy an Enemy.");
        print("Press 'C' to Scan for an Enemy.");
        print("Press 'V' to Show all the Enemies.");
        print("Press 'Z' to Show the item you found.");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            EnemySearch();
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.X))
            {
                EnemyDestruction();
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.C))
                {
                    EnemyScan();
                }
                else
                {
                    if (Input.GetKeyDown(KeyCode.V))
                    {
                        EnemyRoster();
                    }
                    else
                    {
                        if (Input.GetKeyDown(KeyCode.Z))
                        {
                            WeaponSearch();
                        }
                    }
                }
            }
        }
    }

    void EnemySearch()
    {
        for(int i = 0; i < 5; i++)
        {
            enemyDistance = Random.Range(1, 10);

            if (enemyDistance >= 8)
            {
                print("An enemy is far away!");
            }
            else
            {
                if ((enemyDistance >= 4) && (enemyDistance <= 7))
                {
                    print("An enemy is medium range!");
                }
                else if (enemyDistance < 4)
                {
                        print("An enemy is very close!"); 
                }
            }
        }
    }

    void EnemyDestruction()
    {
        while( enemyCount > 0)
        {
            print("There is an enemy! Let´s destroy it!");
            enemyCount--;
        }
    }

    void EnemyScan()
    {
        bool isAlive = false;
        do
        {
            print("Scanning for enemies...");

        } while (isAlive == true);
    }

    void EnemyRoster()
    {
        enemies[0] = "Orc";
        enemies[1] = "Dragon";
        enemies[2] = "Elf";
        enemies[3] = "Human";
        enemies[4] = "Magician";
        enemies[5] = "Demon";

        foreach(string enemy in enemies)
        {
            print(enemy);
        }
    }

    void WeaponSearch()
    {
        weaponId = Random.Range(0, 8);

        switch (weaponId)
        {
            case 1:
                print("You found a Sword.");
                break;
            case 2:
                print("You found an Axe.");
                break;
            case 3:
                print("You found a Dagger.");
                break;
            case 4:
                print("You found a Bow.");
                break;

            default:
                print("You didn´t find anything.");
                break;
        }
    }
}
